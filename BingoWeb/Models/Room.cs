﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BingoWeb.Models
{
    public class Room
    {
        [Key]
        public long Id { get; set; }
        public string  NamePlayer { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
