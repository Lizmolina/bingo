using System;
using System.ComponentModel.DataAnnotations;

namespace BingoWeb.Models
{
    public class GameUser
    {
        [Key]
        public long Id { get; set; }
        public string Link { get; set; }
        public bool  Status { get; set; }

    }
}
