﻿using System;
using Microsoft.EntityFrameworkCore;

namespace BingoWeb.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<GameUser> GameUser { get; set; }
    }
}
