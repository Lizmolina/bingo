import { Component, Inject, OnChanges, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game } from '../games/games.interface';
import { Ball } from './ball.interface';
import { ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';


@Component({
  selector: 'app-paperbox',
  templateUrl: './paperbox.component.html',
  styleUrls: ['./paperbox.component.css']
})
export class PaperboxComponent implements OnChanges {
 public idMesa: string;
 public game: Game;
 public showForm: boolean;
 public FirstForm: boolean;
 public SecondForm: boolean;
 public userTemp: string;
  constructor(private rutaActiva: ActivatedRoute, public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    this.idMesa = this.rutaActiva.snapshot.params.id;
    this.FirstForm = true;
    this.SecondForm = false;
    this.userTemp = '';
    this.game = {
      id: -1,
      name: this.idMesa,
      link: '',
      status: true,
    };
  }

  ngOnChanges() {
  }
  checkGame() {
    this.http.post<Game>(this.baseUrl + 'api/Room/checkgame', this.game).subscribe(result => {
      this.game = {
        id: result['game']['id'],
        name: result['game']['name'],
        link: result['game']['link'],
        status: result['game']['status'],
      };
       if (result['game']['id'] &&  result['game']['status'] === true) {
         this.showForm = true;
       } else{
        alert('el status de esta partida no esta disponible en este momento');
       }
    }, error => alert('El link ingresado no es valido o no esta disponible'));
  }
  saveRoom(form: NgForm) {
      this.FirstForm = false;
      this.SecondForm = true;
      this.userTemp = form.value.name;
  }
}

