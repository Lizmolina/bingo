import { Component, OnInit, Inject, OnChanges } from '@angular/core';
import { Game } from './games.interface';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent  implements OnChanges {

  public games: Game[];
  public game: Game;

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    this.game = {
      id: -1,
      name: '',
      link: '',
      status: true,
    };
    this.refresh();
}
ngOnChanges(){
  this.refresh();
}
refresh() {
  this.http.get<Game[]>(this.baseUrl + 'api/games').subscribe(result => {
    this.games = result;
  }, error => console.error(error));
}
edit(game: Game){
  this.game = game;
}
deleteGame(game: Game){
  this.http.delete(this.baseUrl + 'api/games/' + game.id).subscribe(result => {
    this.refresh();
  }, error => console.error(error));
}
save() {
  if (this.game.id > 0){
    // update
    this.http.put<Game>(this.baseUrl + 'api/games/' + this.game.id, this.game).subscribe(result => {
      this.refresh();
    }, error => console.error(error));
    return;
  }

// insert
    this.http.post<Game>(this.baseUrl + 'api/games', {
      name: this.game.name,
      link: this.game.link,
      status:  this.game.status,
    }).subscribe(result => {
      this.refresh();
    }, error => console.error(error));
  }

  generateLink() {
   if (this.game.name === ''){
      alert('Nombre de partida requerido');
   } else {
    this.game.link = this.baseUrl + 'paperbox/' + this.game.name;
   }
  }

}
