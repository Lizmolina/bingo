import { Component, OnChanges, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router} from '@angular/router';
import { NgForm} from '@angular/forms';
import { from } from 'rxjs';
import { isConstructorDeclaration } from 'typescript';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnChanges {
  isExpanded = false;
  btnlogin: boolean;
  constructor(private router: Router, private jwtHelper: JwtHelperService){
        if (this.isUserAuthenticated) {
          console.log(this.isUserAuthenticated());
         this.btnlogin = false;
        }
          this.btnlogin = true;
  }

  ngOnChanges() {
    if (this.isUserAuthenticated) {
      console.log(this.isUserAuthenticated());
     this.btnlogin = false;
    }
    this.btnlogin = true;
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  isUserAuthenticated() {
    const token: string = sessionStorage.getItem('jwt');
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    } else {
      return false;
    }
  }

  logOut() {
    sessionStorage.removeItem('jwt');
    this.router.navigate(['/']);
  }
}


