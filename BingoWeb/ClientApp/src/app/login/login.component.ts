
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router} from "@angular/router";
import { NgForm} from "@angular/forms";
import { from } from 'rxjs';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
  invalidLogin: boolean;
  constructor(private router: Router, public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) { }

  login(form: NgForm){
    const credentials={
      'email' :form.value.email,
      'password' : form.value.password
    }
    this.http.post('https://localhost:5000/api/auth/login', credentials)
    .subscribe(response =>{
      const token= (<any>response).token;
      sessionStorage.setItem('jwt', token);
      this.invalidLogin = false;
      this.router.navigate(['/games']);
    }, err =>{
      this.invalidLogin = true;
    });
  }
}
