import { Component, OnInit } from '@angular/core';
import { Ball } from '../paperbox/ball.interface';

@Component({
  selector: 'app-rom-admin-game',
  templateUrl: './rom-admin-game.component.html',
  styleUrls: ['./rom-admin-game.component.css']
})
export class RomAdminGameComponent implements OnInit {
  ball: Ball;
  constructor() {
     this.ball = {
      number: 0
     };
  }
  ngOnInit() {
    this.ball = {
      number: 0
     };
  }
  getBall() {
    this.ball.number = this.getRandomInt(1, 70);
  }
    private getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; 
  }
}
