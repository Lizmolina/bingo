﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BingoWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BingoWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoomController : Controller
    {
        private readonly DataBaseContext _context;
        public RoomController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Room>>> GetGames()
        {
            return await _context.Rooms.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Game>> PostRoom(Room room)
        {
            _context.Rooms.Add(room);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGame", new { id = room.Id }, room);
        }

        [HttpPost, Route("checkgame")]
        public IActionResult GameExist(Game game) {
            IActionResult response = NotFound();
            var _game = FindGame(game);
            if (_game != null)
            {
                response = Ok(new
                {
                    game=_game
                });
            }
                return response;
        }

        Game FindGame(Game game)
        {
            return _context.Games.Where(e => e.Name == game.Name).FirstOrDefault();
        }
    }
}
